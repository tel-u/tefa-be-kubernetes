require('dotenv').config();
const configs = require('./configs/configs');
const bodyParser = require('body-parser');
const express = require('express');
const os = require('os');
const fs = require('fs').promises;
const serveIndex = require('serve-index');

const appConfig = configs.get('app');

async function main(){
  const router = express();

  router.use('/assets', serveIndex(appConfig.volumeMountingPath), express.static(appConfig.volumeMountingPath));
  router.use(bodyParser.urlencoded({ extended: false}));
  router.use(bodyParser.json());


  router.get('/health', function(req, res) {
    const responseBody = {
      status: 'OK',
      data: {
        hostname: os.hostname(),
        service: appConfig.name,
        port: appConfig.port,
        client: req.headers['user-agent'],
        configurations: {...appConfig}
      },
      message: 'application is running properly'
    };

    res.status(200).json(responseBody);
  });

  router.post('/files', async function(req, res) {
    const filename = req.body.filename || new Date().getTime().toString();
    const content = req.body.content || 'lorem ipsum dolor';

    try {
      await fs.writeFile(`${appConfig.volumeMountingPath}/${os.hostname()}-${filename}`, content);
      const responseBody = {
        status: 'CREATED',
        data: {
          ...req.body
        },
        message: 'a new file has been successfuly added'
      };

      res.status(201).json(responseBody);
      
    } catch (error) {
      const responseBody = {
        status: 'UNEXPECTED_ERROR',
        message: error.message
      };

      res.status(500).json(responseBody);
    } finally{
      res.end();
    }
  });

  const server = router.listen(appConfig.port, function() {
    console.info('application is running on port', appConfig.port);
  });

  process.on('SIGTERM', closeServer(server));
  process.on('SIGINT', closeServer(server));
}

function closeServer(server) {
  return function() {
    server.close(function (err) {
      if (err) {
        console.error(err.message);
      }
      console.info('server is gracefully shutdown')
    });
  };
}

main();