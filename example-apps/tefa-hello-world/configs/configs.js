const configs = {
  app: {
    name: process.env.APP_NAME,
    port: process.env.APP_PORT || Number(9000).toString()
  }
};

function get(name) {
  return configs[name];
}

module.exports = {
  get: get
}
