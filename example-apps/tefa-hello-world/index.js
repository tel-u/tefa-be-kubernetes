require('dotenv').config();
const configs = require('./configs/configs');
const express = require('express');
const os = require('os')

const appConfig = configs.get('app');

async function main(){
  const router = express();

  router.set('view engine', 'ejs');

  router.get('/health', function(req, res) {
    res.status(200).send('OK');
  });

  router.get('/', function(req, res) {
    res.status(200).render('index', {
      title: appConfig.name,
      hostname: os.hostname()
    });
  });

  const server = router.listen(appConfig.port, function() {
    console.info('application is running on port', appConfig.port);
  });

  process.on('SIGTERM', closeServer(server));
  process.on('SIGINT', closeServer(server));
}

function closeServer(server) {
  return function() {
    server.close(function (err) {
      if (err) {
        console.error(err.message);
      }
      console.info('server is gracefully shutdown')
    });
  };
}

main();